describe('Cypress Change Image Signature', () => {
    it.only('change image signature', () => {
        cy.visit('https://privy.id/')
        cy.contains('Login').click({
            force: true
        })
        cy.contains('Log In').should('exist')
        cy.get('input[name="user[privyId]"]').type('ZD8298', {
            force: true
        })
        cy.contains('CONTINUE').click()
        cy.get('input[name="user[secret]"]').type('Pram2022', {
            force: true
        })
        cy.contains('CONTINUE').click()
            // cy.get('#v-step-0').click()
        cy.contains('Change My Signature Image').click()
        cy.contains('Add Signature').click({ force: true })
        cy.contains('Image').click()
        const imgName = 'signature.png';

        cy.get('#__BVID__230').click()
        cy.get('div > [type="file"]').attachFile(imgName)
        cy.contains('Crop').click()
        cy.contains('Apply').click()

        cy.get('#__BVID__231 > div > div > div').click();
        cy.get('div > input[type="file"]').attachFile(imgName);
        cy.contains('Crop').click();
        cy.contains('Apply').click();
        cy.contains('Save').click();
        cy.wait(2000)
        cy.visit('https://app.privy.id/')
        cy.contains('Change My Signature Image').click()
        cy.contains('Set as default').click()
        cy.get('.signature-item:first-child > .signature-item__control > button:first-child > span').click({ force: true })

    })

})